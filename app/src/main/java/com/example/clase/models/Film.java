package com.example.clase.models;

import com.google.gson.annotations.SerializedName;

public class Film {
    @SerializedName("id")
    private String id;

    @SerializedName("original_title_romanised")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("description")
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
