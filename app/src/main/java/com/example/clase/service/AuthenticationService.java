package com.example.clase.service;

import com.example.clase.constants.AuthenticationAPI;
import com.example.clase.models.AuthenticationResponse;
import com.example.clase.models.Login;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthenticationService {

    @POST(AuthenticationAPI.AUTHENTICATION_ENDPOINT)
    Call<AuthenticationResponse> getUsers(@Body Login login);
}
