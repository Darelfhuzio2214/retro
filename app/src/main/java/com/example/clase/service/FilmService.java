package com.example.clase.service;

import com.example.clase.constants.FilmAPI;
import com.example.clase.models.Film;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface FilmService {
    @GET(FilmAPI.FILMS_ENDPOINT)
    Call<List<Film>> getFilms();

}
