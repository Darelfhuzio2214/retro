package com.example.clase.ui;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.example.clase.R;
import com.example.clase.adapter.AuthenticationAdapter;
import com.example.clase.adapter.FilmAdapter;
import com.example.clase.models.AuthenticationResponse;
import com.example.clase.models.Film;
import com.example.clase.models.Login;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //MostrarFilms();
        mostrarUsuarios();
    }


    private void MostrarFilms() {
        FilmAdapter filmAdapter = new FilmAdapter();
        Call<List<Film>> call = filmAdapter.getFilms();
        call.enqueue(new Callback<List<Film>>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<List<Film>> call, Response<List<Film>> response) {

                List<Film> datos = response.body();
                datos.forEach((item) -> Log.e("test", "nombre " + item.getTitle()));
            }

            @Override
            public void onFailure(Call<List<Film>> call, Throwable t) {
                Log.e("test", "no se pudo traer los datos");
            }
        });
    }


    private void mostrarUsuarios() {
        AuthenticationAdapter authenticationAdapter = new AuthenticationAdapter();
        Login log = new Login("alex@gmail.com", "12345678");
        Call<AuthenticationResponse> call = authenticationAdapter.getUsers(log);
        call.enqueue(new Callback<AuthenticationResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {

           //    token = response.body().getToken();

                Log.e("test", response.body().getToken());

            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                Log.e("test", "no se pudo traer los datos");
            }
        });
    }
}