package com.example.clase.adapter;

import com.example.clase.constants.FilmAPI;
import com.example.clase.models.Film;
import com.example.clase.service.FilmService;

import java.util.List;

import retrofit2.Call;

public class FilmAdapter extends BaseAdapter implements FilmService {

    private FilmService filmService;

    public FilmAdapter() {
        super(FilmAPI.BASE_GHIBLI_URL);
        filmService  = createService(FilmService.class);
    }

    @Override
    public Call<List<Film>> getFilms() {
        return filmService.getFilms();
    }
}
