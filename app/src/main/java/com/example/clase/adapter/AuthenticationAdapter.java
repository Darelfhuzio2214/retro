package com.example.clase.adapter;

import com.example.clase.constants.AuthenticationAPI;
import com.example.clase.models.AuthenticationResponse;
import com.example.clase.models.Login;
import com.example.clase.service.AuthenticationService;

import java.util.List;

import retrofit2.Call;

public class AuthenticationAdapter extends BaseAdapter implements AuthenticationService {
    private AuthenticationService authenticationService;

    public AuthenticationAdapter() {
        super(AuthenticationAPI.BASE_URL);
        authenticationService = createService(AuthenticationService.class);
    }

    @Override
    public Call<AuthenticationResponse> getUsers(Login login) {
        return authenticationService.getUsers(login);
    }
}
