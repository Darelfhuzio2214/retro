package com.example.clase.constants;

public class FilmAPI {


    //BASE URL
    public static final String BASE_GHIBLI_URL = "https://ghibliapi.herokuapp.com";


    //FILMS ENDPOINT
    public static final String FILMS_ENDPOINT = "/films";

}
